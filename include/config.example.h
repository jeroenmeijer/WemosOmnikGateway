#include <Arduino.h>
// Update these with values suitable for your MQTT server and account

#define MQTT_SERVER "ip or hostname of your mqtt server"
#define MQTT_PORT 8883
#define MQTT_USERNAME "username"
#define MQTT_PASSWORD "password"
#define MQTT_CLIENTID "clientid (random string)"
#define MQTT_RESETMSG "{\"online\":true, \"reset\":true, \"version\":\"" __DATE__ "T" __TIME__ "\"}"
#define MQTT_OUTTOPIC "Solar/Omnik"

// Station
#define HOMESSID "ssid house wifi"
#define HOMEPASSWORD "passwoord house wifi"

// omnik
#define OMNIKSSID "ssid omnik inverter"
#define OMNIKPASSWORD "wifi password omnik inverter"
byte omnikCommand[] = {0x68, 0x02, 0x40, 0x30,  // leader, don't change
                       0xc5, 0x2a, 0xc4, 0x5f,  // serial in reverse hex! so 1606691525 => 0x5FC42AC5 => c52ac45f
                       0xc5, 0x2a, 0xc4, 0x5f,  // repeat
                       0x01, 0x00, 0x95, 0x16}; // trailer, don't change
