/*! \mainpage Wemos Omnik Gateway
 *
 * \section intro_dec Introduction
 *
 * This firmware for a Wemos D1 mini or any other suitable ESP8266 board
 * periodically connects to the Access Point of an Omink Inverter to acquire
 * the data that we want. We use a reverse engineered non-web protocol for
 * that. I used this code as a starting point.
 * https://github.com/arjenv/omnikstats
 * It then disconnect, connects to the home network and publishes said data as
 * a JSON formatted MQTT message.
 */

/**
 * @file WemosOmnikGateway2.cpp
 * @brief Contains all code except included libraries
 * @details Note that a config.h file with credentials is required. Also
 * this code assumes an MQTTS capable broker. FOr a non secure broker
 * change the WiFiClient here and the port number in config.h
 * \n
 * Blink codes for error finding \n
 * Ran loop 1 \n
 * Ran setup code SETUP 2 \n
 * Can't find WiFI access point 3 \n
 * Can't find MQTT broker 4 \n
 * Can't find Ominik WiFI 5 \n
 * Cannot connect to Omnik over TCPIP 6 \n
 * No Omnik data 7 \n
 * Incomplete Omnik data 8 \n
 */

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <WiFiClientSecure.h>

// PubSubClient.h modified
// If the reset message makes it but the actual data does not, the problem is
// the max size of the message, as defined in PubSubClient.h. Set there to 512
#include <PubSubClient.h>

// You have to make your config file yourself. There is an example file in the include folder
#include "config.h"

/** @defgroup home Home network
 *  Data about the home network
 *  @{
 */
/** @brief SSID of the home network */
const char *homeSsid = HOMESSID;
/** @brief Password of the home network */
const char *homePassword = HOMEPASSWORD;
/** @} */ // end of home

unsigned long lastReconnectAttempt = 0;

/** @defgroup led LED
 *  LED
 *  @{
 */
/// LED pin
#define blink_pin LED_BUILTIN
/// LED on time in ms
#define time_on 150 
/// LED off time in ms
#define time_off 300

/// Success. One blink after each loop
#define SUCCESS 1
 /// setup done
#define SETUP 2
/// Home access point cannot be contacted
#define NO_AP_WIFI 3
/// MQTT broker cannot be contacted
#define NO_MQTT 4
/// Omink access point cannot be contacted
#define NO_OMNIK_WIFI 5
/// Cannor connect to Omink over TCPIP
#define NO_OMNIK_SESSION 6
/// Omnik does not reply
#define NO_OMNIK_DATA 7
/// Omnik only partially replies
#define NO_OMNIK_FULL 8

/**
 * Blink the LED x times. Default we use a short blink, but to represent a 0 or
 * error conditions, we use long blinks
 * @param blinks number of flashes
 * @param shrt if true use short blinks
 */
void blink(int blinks, boolean shrt)
{
	for (int blink_count = 1; blink_count <= blinks; blink_count++)
	{
		digitalWrite(blink_pin, LOW);		 // turn the LED on
		delay(shrt ? time_on : time_on * 5); // wait for t milliseconds
		digitalWrite(blink_pin, HIGH);		 // turn the LED off
		delay(time_off);					 // wait for t milliseconds
		if ((blink_count % 3) == 0)
			delay(time_off); // extra per 3
	}
	delay(time_off * 5); // pause between digits
}
/** @} */ // end of LED


/** @defgroup wifi WiFi network
 *  WiFi network
 *  @{
 */
/// WiFi
WiFiClient espClient;
/// Secure WiFi
WiFiClientSecure espClientSecure;

/**
 * Connect to a WiFi AP. Wait for a maximum of 10 seconds
 * @param ssid SSID of the WiFi network to connect to
 * @param password of the WiFi network to connect to
 */
boolean startWifi(const char *ssid, const char *password)
{
	Serial.print("function startwifi, Attempting to connect to SSID: ");
	Serial.println(ssid);
	// set station mode
	WiFi.mode(WIFI_STA);
	espClientSecure.setInsecure();

	// attempt to connect to Wifi network:
	if (WiFi.status() != WL_CONNECTED)
	{
		Serial.println("function startwifi, WiFi.begin");
		WiFi.begin(ssid, password);
	}

	int c = 100;
	while (c-- && WiFi.status() != WL_CONNECTED)
		delay(100);
	if (WiFi.status() != WL_CONNECTED)
	{
		Serial.println("function startwifi, Failed to connect");
		WiFi.mode(WIFI_OFF);
		return false;
	}
	else
	{
		Serial.print("function startwifi, Connected to: ");
		Serial.print(WiFi.SSID());
		Serial.print(", IP address: ");
		Serial.println(WiFi.localIP());
	}
	return true;
}
/** @} */ // end of Wifi

/** @defgroup mqtt MQTT broker
 *  MQTT broker data
 *  @{
 */

PubSubClient pubSubClient(espClientSecure);

/// MQTT server FQN or IP address
const char *mqtt_server = MQTT_SERVER;
/// MQTT port (usually 8883 for MQTTS, 1883 for insecure)
const int mqtt_port = MQTT_PORT;
/// MQTT username
const char *mqtt_username = MQTT_USERNAME;
/// MQTT password
const char *mqtt_password = MQTT_PASSWORD;
/// MQTT client ID. Random string
const char *mqtt_clientID = MQTT_CLIENTID;
/// MQTT topic to publish data on
const char *mqtt_outTopic = MQTT_OUTTOPIC;

/**
 * Empty MQTT callback function, as we don't accept any incoming commands.
 */
void callback(char *topic, byte *payload, unsigned int length)
{
}

/**
 * connect to the MQTT broker, assume WiFi up and running.
 * Retry for max 5 seconds
 */
void reconnect()
{
	if (WiFi.status() != WL_CONNECTED)
		return;
	if (!pubSubClient.connected())
	{
		unsigned long now = millis();
		// do not try faster than once per 5 seconds
		if (now - lastReconnectAttempt > 5000)
		{
			Serial.println("function reconnect, MQTT (re)connecting...");
			lastReconnectAttempt = now;
			// Attempt to reconnect
			if (pubSubClient.connect(mqtt_clientID, mqtt_username, mqtt_password))
			{
				Serial.println("function reconnect, MQTT connected!");
				lastReconnectAttempt = 0;
			}
		}
		else
		{
			delay(1); // allow the OS some time
		}
	}
}

/**
 * Connect to home network and publish messageBuf. Switch off the WiFi when done.
 */
void publishResult(const char *messageBuf)
{
	Serial.println("function publishResult, connect to home AP");
	WiFi.mode(WIFI_OFF);
	delay(1000);

	// it's pretty useless to go on without a home network, so keep on trying
	// this will also avoid a nasty hang in the reconnect
	while (!startWifi(homeSsid, homePassword))
	{
		// indicate error
		blink(NO_AP_WIFI, true);
	}

	Serial.print("function publishResult, publish:");
	Serial.println(messageBuf);
	// try to connect
	unsigned long startPubSubTime = millis();

	while (!pubSubClient.connected() && (millis() - startPubSubTime < 15000))
		reconnect();
	// if still not connected after 15 seonds, give up
	if (pubSubClient.connected())
	{
		Serial.println("function publishResult, MQTT connected");
		pubSubClient.publish(mqtt_outTopic, messageBuf, true);
		Serial.println("function publishResult, MQTT published");
		pubSubClient.disconnect();
		Serial.println("function publishResult, MQTT disconnected");
	}
	else
	{
		Serial.println("function publishResult, no connection to MQTT server");
		blink(NO_MQTT, true);
	}
	WiFi.mode(WIFI_OFF);
}
/** @} */ // end of MQTT


/** @defgroup omnik Omnik network
 *  Data about the Omnik network
 *  @{
 */
/// SSID of the Omnik inverter
const char *omnikSsid = OMNIKSSID;
/// Password of the Omnik inverter
const char *omnikPassword = OMNIKPASSWORD;
/// IP of omnik
byte omnikIp[] = {10, 10, 100, 254};

/**
 * Convert unsigned to string with d decimals, d being 0, 1 or 2.
 */
char *mstr(char *buf, unsigned int v, int d)
{
	if (d == 0)
	{
		snprintf(buf, 16, "%u", v);
	}
	else if (d == 1)
	{
		snprintf(buf, 16, "%u.%01u", v / 10, v % 10);
	}
	else if (d == 2)
	{
		snprintf(buf, 16, "%u.%02u", v / 100, v % 100);
	}
	return buf;
}


/**
 * actual work (query Omnik, push result as JSON in a MQTT message
 */
void doWork()
{
	byte inBuffer[100];
	char messageBuf[256];
	char buf[16];

	Serial.println("function doWork, connect to Omnik AP");
	if (!startWifi(omnikSsid, omnikPassword))
	{
		Serial.println("function doWork, not connected to Omnik Wifi");
		publishResult("{\"error\":\"not connected to Omnik WiFi\"}");
		blink(NO_OMNIK_WIFI, true);
		return;
	}

	// connect to ip of omnik
	if (!espClient.connect(omnikIp, 8899))
	{
		Serial.println("function doWork, not connected to inverter IP");
		publishResult("{\"error\":\"not connected to inverter IP\"}");
		blink(NO_OMNIK_SESSION, true);
		return;
	}

	delay(100);

	// send command
	Serial.println("function doWork, write to Omnik");
	byte command[] = {0x68, 0x02, 0x40, 0x30, 0xc4, 0x2a, 0xc4, 0x5f, 0xc4, 0x2a, 0xc4, 0x5f, 0x01, 0x00, 0x95, 0x16}; // in reverse hex! so 1606691524 => 0x5FC42AC4 => c4
	espClient.write((const uint8_t *)command, (uint8_t)16);

	// wait until data is available
	Serial.println("function doWork, wait...");
	for (int ci = 0; ci < 1000; ci++)
	{
		if (espClient.available() > 0)
			break;
		delay(2);
	}
	if (espClient.available() == 0)
	{
		Serial.println("function doWork, no data");
		espClient.stop();
		publishResult("{\"error\":\"no data\"}");
		blink(NO_OMNIK_DATA, true);
		return;
	}

	// read the buffer
	Serial.println("function doWork, read");
	for (unsigned bi = 0; bi < sizeof(inBuffer); bi++)
	{
		for (int ci = 0; ci < 1000; ci++)
		{
			if (espClient.available() > 0)
				break;
			delay(2);
		}
		if (espClient.available() == 0)
		{
			Serial.println("function doWork, insufficient data");
			espClient.stop();
			publishResult("{\"error\":\"insufficient data\"}");
			blink(NO_OMNIK_FULL, true);
			return;
		}
		inBuffer[bi] = espClient.read();
	}
	espClient.stop();

	Serial.println("function doWork, reformatting to JSON");

	// {"acPowerNow":133,"acVoltageNow":233.6,"acCurrentNow":0.8,"dcVoltage":373.6,"dcCurrent":0.3,"frequency":49.98,"energyToday":19.99,"energyTotal":5519.7,"temperatureHeatsink":32.2}

	strcpy(messageBuf, "{\"acPowerNow\":");
	strcat(messageBuf, mstr(buf, (inBuffer[59] * 256 + inBuffer[60]), 0));
	strcat(messageBuf, ",\"acVoltageNow\":");
	strcat(messageBuf, mstr(buf, (inBuffer[51] * 256 + inBuffer[52]), 1));
	strcat(messageBuf, ",\"acCurrentNow\":");
	strcat(messageBuf, mstr(buf, (inBuffer[45] * 256 + inBuffer[46]), 1));
	strcat(messageBuf, ",\"dcVoltage\":");
	strcat(messageBuf, mstr(buf, (inBuffer[33] * 256 + inBuffer[34]), 1));
	strcat(messageBuf, ",\"dcCurrent\":");
	strcat(messageBuf, mstr(buf, (inBuffer[39] * 256 + inBuffer[40]), 1));
	strcat(messageBuf, ",\"frequency\":");
	strcat(messageBuf, mstr(buf, (inBuffer[57] * 256 + inBuffer[58]), 2));
	strcat(messageBuf, ",\"energyToday\":");
	strcat(messageBuf, mstr(buf, (inBuffer[69] * 256 + inBuffer[70]), 2));
	strcat(messageBuf, ",\"energyTotal\":");
	strcat(messageBuf, mstr(buf, (inBuffer[73] * 256 + inBuffer[74]), 1));
	strcat(messageBuf, ",\"temperatureHeatsink\":");
	strcat(messageBuf, mstr(buf, (inBuffer[31] * 256 + inBuffer[32]), 1));
	strcat(messageBuf, "}");

	publishResult(messageBuf);
}
/** @} */ // end of Omnik

/** @defgroup arduino Arduino
 *  Arduino setup and loop
 *  @{
 */
/**
 * Main entry point of the code.
 */
void setup(void)
{
	pinMode(blink_pin, OUTPUT);
	blink(SUCCESS, true);
	Serial.begin(115200);

	// ensure clean state WiFi
	WiFi.persistent(false);
	WiFi.mode(WIFI_OFF);
	delay(1000);

	espClientSecure.setInsecure();
	pubSubClient.setServer(mqtt_server, mqtt_port);
	pubSubClient.setCallback(callback);
	publishResult(MQTT_RESETMSG);
	// Wifi is off after publishResult()
	blink(SETUP, true);
}

/**
 * Main loop. This is automatically called repeatedly by the Arduiono framework
 */
void loop(void)
{
	Serial.println("function loop, pausing 20 seconds ======================");
	delay(20000);
	doWork();
	blink(SUCCESS, true);
}
/** @} */ // end of Arduino